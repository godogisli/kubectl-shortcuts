# Useful commands to avoid typing on long kubectl stuff.

The file "Config" contains the namespace that's used by default.


| Command      | Description                       |
---------------|-----------------------------------|
| kgp          | Get pod list                      |
| ktest        | Switch to the test cluster        |
| kprod        | Switch to the prod cluster        |
| kmini        | Switch to the minikube cluster    |
| kc           | Show which cluster you are using  |
| kgd          | Get deployments                   |
| kdd $dep     | Describe deployment               |
| kdyaml $dep  | Get yaml for deployment           |
| kdp $pod     | Describe pod                      |
| klogs $pod   | Show logs for pod                 |
| kexec	$POD   | Start a shell on the given pod    |
| kns	$NS      | Change the default namespace      |
| kgs	         | Get services                      |
| kgn	         | Get namespaces                    |
| kds	$SRV     | Describe service                  |
| ksyaml	$SRV | Get yaml output for service       |
| kiyaml	$SRV | Get yaml output for ingress       |
| kdelp	$POD   | Delete pod $POD                   |
| kdedit	$DEP | Edit deployment $DEP              |
| ksedit	$SER | Edit service $SER                 |
| kiedit	$ING | Edit deployment $ING              |
